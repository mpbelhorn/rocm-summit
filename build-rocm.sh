#!/bin/bash

# TODO: Expand this script to cover the full ROCm stack (see https://github.com/RadeonOpenCompute/ROCm)
# Nominal build procedures for each package can be found in their source trees,
# in particular among build scripts for generating debian packages. Debian
# packages are, as of 2019-11-05, AMD's defacto preferred distribution method.
#
## Core packages
# - ROCk Kernel Driver: https://github.com/RadeonOpenCompute/ROCK-Kernel-Driver/blob/36767d92bdfecb49f2c5f112285b483549420267/scripts/package/builddeb
# - ROCr Runtime: https://github.com/RadeonOpenCompute/ROCR-Runtime/tree/master/src
# - ROCt THUNK interface: https://github.com/RadeonOpenCompute/ROCT-Thunk-Interface/tree/roc-2.9.0
#   -- version coupled to ROCk.
#
## Support packages
# - ROC-smi System Management Interface: https://github.com/RadeonOpenCompute/ROC-smi/tree/roc-2.9.0
# - ROCm CMake: https://github.com/RadeonOpenCompute/rocm-cmake/tree/roc-2.9.0
# - rocminfo: https://github.com/RadeonOpenCompute/rocminfo/tree/roc-2.9.0
# - RocBandwidthTest: https://github.com/RadeonOpenCompute/rocm_bandwidth_test/tree/roc-2.9.0
#
## Dev Tools
# - hcc: https://github.com/RadeonOpenCompute/hcc/tree/roc-hcc-2.9.0
# - hip: https://github.com/ROCm-Developer-Tools/HIP/blob/roc-2.9.0/install.sh
# - ROCm Device Libs: https://github.com/RadeonOpenCompute/ROCm-Device-Libs/tree/roc-hcc-2.9.0
### OCL Support
# - ROCm OpenCL Runtime: https://github.com/RadeonOpenCompute/ROCm-OpenCL-Runtime/tree/roc-2.9.0
# - ROCm OpenCL Driver: https://github.com/RadeonOpenCompute/ROCm-OpenCL-Driver/tree/roc-2.9.0
# - ROCm LLVM OCL: https://github.com/RadeonOpenCompute/llvm/tree/roc-ocl-2.9.0
# - ROCm LLVM HCC: https://github.com/RadeonOpenCompute/llvm/tree/roc-hcc-2.9.0
# - ROCm Clang: https://github.com/RadeonOpenCompute/clang/tree/roc-2.9.0


set -e

CLEAN_BUILD=false
CLEAN_DEPLOY=false

ROCM_VERSION="3.0.0"
INSTALL_ROOT_DIR="/sw/.testing/belhorn/summit/rocm"
PATCH_ROOT_DIR="/sw/.testing/belhorn/summit/rocm"
BUILD_ROOT_DIR="/gpfs/alpine/stf007/proj-shared/belhorn/rocm"

LLVM_VERSION="10.0.0"
CUDA_MODULE_NAME="cuda/10.1.168"
COMPUTE_CAPABILITY="70"
THREADS=24 # Set lower if OOM errors occur during linking.

BASE_PREFIX="${INSTALL_ROOT_DIR}/${ROCM_VERSION}"
BUILD_PREFIX="${BUILD_ROOT_DIR}/${ROCM_VERSION}"
MODULE_PREFIX="${BASE_PREFIX}/modulefiles"
ROCM_PREFIX="${BASE_PREFIX}/opt/rocm"
LLVM_PREFIX="${ROCM_PREFIX}/llvm"

REPO_BIN="${INSTALL_ROOT_DIR}/utils/bin/repo"
GCC_BIN="${LLVM_PREFIX}/bin/gcc"
GXX_BIN="${LLVM_PREFIX}/bin/g++"
CLANG_BIN="${LLVM_PREFIX}/bin/clang"
CLANGXX_BIN="${LLVM_PREFIX}/bin/clang++"
HCC_BIN="${ROCM_PREFIX}/hcc/bin/hcc"


###############
# Script Utilities

function apply_patch_string() {
  # MUST use "double-quotes" around passed argument strings.
  set +e
  if ! patch -R -p1 -s -f --dry-run <<< "$1" 2>/dev/null; then
    set -e
    patch -p1 <<< "$1"
  fi
  set -e
}

function apply_patch_file() {
  if [ -f "$1" ]; then
    set +e
    if ! patch -R -p1 -s -f --dry-run < "$1" 2>/dev/null; then
      set -e
      patch -p1 < "$1"
    fi
    set -e
  fi
}

function log_section() {
  printf "\n=> $1\n\n"
}

function log_info() {
  echo "==> $1"
}

function append_path() {
    if [ ${#} -eq 0 ]
    then
        echo "\`append_path\` appends elements separated by colons to a \`bash\` variable " >&2
        echo "usage: append_path <variable> <element> [element ...]" >&2
        return 1
    fi
    variable="${1}"

    shift
    export ${variable}="${!variable:+${!variable}:}$(IFS=:; echo "${*}")"
}

function prepend_path() {
    if [ ${#} -eq 0 ]
    then
        echo "\`prepend_path\` inserts elements separated by colons to a \`bash\` variable " >&2
        echo "usage: prepend_path <variable> <element> [element ...]" >&2
        return 1
    fi
    variable="${1}"

    shift
    export ${variable}="$(IFS=:; echo "${*}")${!variable:+:${!variable}}"
}

function show_build_env() {
  echo "PATH=${PATH}"
  echo "PKG_CONFIG_PATH=${PKG_CONFIG_PATH}"
  echo "LD_LIBRARY_PATH=${LD_LIBRARY_PATH}"
  echo "LD_RUN_PATH=${LD_RUN_PATH}"
  echo "LIBRARY_PATH=${LIBRARY_PATH}"
  echo "CMAKE_PREFIX_PATH=${CMAKE_PREFIX_PATH}"
  echo "CMAKE_MODULE_PATH=${CMAKE_MODULE_PATH}"
  # TODO: Verify state of CPATH, CPLUS_INCLUDE_PATH, etc as necessary.
}

###############
# Log start of new build

log_section "========================\nNew build started $(\date --iso-8601=minutes)\n"

###############
# Setup Google Repo tool

log_section "Setting up repo tool and sources."

mkdir -p "${INSTALL_ROOT_DIR}/utils/bin"
[ ! -f "${REPO_BIN}" ] \
  && curl https://storage.googleapis.com/git-repo-downloads/repo > "${REPO_BIN}" \
  && chmod a+x "${REPO_BIN}"

# Fetch all the source code
mkdir -p "${BUILD_PREFIX}/src" && cd "${BUILD_PREFIX}/src"
[ ! -d "./.repo" ] \
  && ${REPO_BIN} init -u https://github.com/RadeonOpenCompute/ROCm.git -b roc-${ROCM_VERSION} \
  && ${REPO_BIN} sync


###############
# Clean build and install areas and initialize environment

log_section "Setting up initial build environment."

# Without explicit instruction, clean the build directory
[ ${CLEAN_BUILD:-true} = true ]           \
  && log_info "Cleaning build directory." \
  && rm -fr "${BUILD_PREFIX}/build" 

# Without explicit instruction, keep the install directory
[ ${CLEAN_DEPLOY:-false} = true ]           \
  && log_info "Cleaning install directory." \
  && rm -fr "${ROCM_PREFIX}" 

# Load the initial environment
module purge
module load ${CUDA_MODULE_NAME} cmake git
module -w 10 --redirect list

GCC_LIB64_DIR="${LLVM_PREFIX}/lib64"
ROCM_LIB_DIR="${ROCM_PREFIX}/lib"
ROCM_LIB64_DIR="${ROCM_PREFIX}/lib64"
ROCR_LIB_DIR="${ROCM_PREFIX}/hsa/lib"
CUDA_HOME="${OLCF_CUDA_ROOT}"
export CUDA_PATH="${CUDA_HOME}"

TOOLCHAIN_RPATH="-Wl,-rpath,${LLVM_PREFIX}/lib64 -L${LLVM_PREFIX}/lib64"
ROCM_LIB_RPATH="-Wl,-rpath,${ROCM_PREFIX}/lib -L${ROCM_PREFIX}/lib"
ROCM_LIB64_RPATH="-Wl,-rpath,${ROCM_PREFIX}/lib64 -L${ROCM_PREFIX}/lib64"
ROCR_RPATH="-Wl,-rpath,${ROCM_PREFIX}/hsa/lib -L${ROCM_PREFIX}/hsa/lib"

CUDA_LIB_DIR="${CUDA_HOME}/lib64"
CUDA_RPATH="-Wl,-rpath,${CUDA_LIB_DIR} -L${CUDA_LIB_DIR}"

unset LIBPATH
unset CMAKE_MODULE_PATH
show_build_env


###############
# Private GCC for LLVM

log_section "Building own GCC."

gcc_version=5.5.0
GCC_SRC_ROOT="${BUILD_PREFIX}/src/gcc"
GCC_SRC="${GCC_SRC_ROOT}/gcc-${gcc_version}"
GCC_BUILD="${BUILD_PREFIX}/build/gcc-${gcc_version}"
GNU_FTP_URL="https://ftp.gnu.org/gnu"
GNU_GCC_URL="${GNU_FTP_URL}/gcc/gcc-${gcc_version}"
GCC_ARCHIVE="gcc-${gcc_version}.tar.gz"

## Fetch sources
mkdir -p "${GCC_SRC_ROOT}"
cd "${GCC_SRC_ROOT}"
[ ! -f "${GCC_ARCHIVE}" ] && wget "${GNU_GCC_URL}/${GCC_ARCHIVE}"
[ ! -f "${GCC_ARCHIVE}.sig" ] && wget "${GNU_GCC_URL}/${GCC_ARCHIVE}.sig"
[ ! -f "gnu-keyring.gpg" ] && wget "${GNU_FTP_URL}/gnu-keyring.gpg"

# Abort if archive is corrupt.
if ! gpg --verify --no-default-keyring \
         --keyring ./gnu-keyring.gpg ${GCC_ARCHIVE}.sig; then
  log_info "Invalid signature"
  exit 1
fi

# Unpack source archive
[ ! -d "${GCC_SRC}" ] && tar -xvf "${GCC_ARCHIVE}"

# Fetch prereqs if missing.
if [[ ! -L "${GCC_SRC}/gmp"  || \
      ! -L "${GCC_SRC}/mpc"  || \
      ! -L "${GCC_SRC}/mpfr" ]]; then
  cd "${GCC_SRC}"
  ./contrib/download_prerequisites
fi

## Build and install GCC toolchain
if ( "${GCC_BIN}" --version > /dev/null 2>&1 && \
     "${GXX_BIN}" --version > /dev/null 2>&1); then
  log_info "GCC already installed; skipping."
else
  mkdir -p "${GCC_BUILD}"
  cd "${GCC_BUILD}"
  # # Hardcode library rpaths in default specs.
  # _gcc_specs='%{shared:-Wl,-rpath -Wl,$(DESTDIR)/lib64}'
  # _gcc_specs+='%{!shared:-Wl,-rpath -Wl,$(DESTDIR)/lib64}'
  #                      --with-specs="${_gcc_specs}"           \
  "${GCC_SRC}/configure" --prefix="${LLVM_PREFIX}"              \
                         --enable-offload-targets=nvptx-none    \
                         --with-cuda-driver="${CUDA_HOME}"      \
                         --enable-languages="c,c++,fortran,lto" \
                         --disable-multilib CFLAGS=-m64
  make -j "${THREADS:-16}"
  make install
fi


###############
# LLVM

log_section "Installing 'AMD LLVM/Clang'."

cd "${BUILD_PREFIX}/src/llvm_amd-stg-open/llvm"  # ROCM v3.X
mkdir -p "${BUILD_PREFIX}/build/llvm_amd-stg-open" # ROCM v3.X
cd "${BUILD_PREFIX}/build/llvm_amd-stg-open"  # ROCM v3.X

if ( "${CLANG_BIN}" --version > /dev/null 2>&1 && \
     "${CLANGXX_BIN}" --version > /dev/null 2>&1); then
  log_info "LLVM/Clang already installed; skipping."
else
  # TODO: Rather than use CXX_LINK_FLAGS, consider setting LD_RUN_PATH and
  #       -DCMAKE_INSTALL_RPATH
  cmake -DCMAKE_INSTALL_PREFIX="${LLVM_PREFIX}"            \
        -DGCC_INSTALL_PREFIX="${LLVM_PREFIX}"              \
        -DCMAKE_C_COMPILER="${GCC_BIN}"                    \
        -DCMAKE_CXX_COMPILER="${GXX_BIN}"                  \
        -DCMAKE_CXX_LINK_FLAGS="${TOOLCHAIN_RPATH}"        \
        -DCMAKE_BUILD_TYPE=Release                         \
        -DLLVM_TARGETS_TO_BUILD="PowerPC;X86;NVPTX;AMDGPU" \
        -DLLVM_ENABLE_PROJECTS="clang;clang-tools-extra;compiler-rt;libunwind;lld;lldb;openmp;parallel-libs" \
        "${BUILD_PREFIX}/src/llvm_amd-stg-open/llvm"
  make -j "${THREADS:-16}"
  make install
fi


###############
# Setup the ROCm build environment

log_section "Setting up ROCm build environment."

prepend_path PATH "${ROCM_PREFIX}/bin" "${LLVM_PREFIX}/bin"
prepend_path PKG_CONFIG_PATH "${ROCM_PREFIX}/share/pkgconfig"
prepend_path LD_LIBRARY_PATH "${ROCM_LIB64_DIR}" "${ROCM_LIB_DIR}" "${GCC_LIB64_DIR}"
# The magic variable to get CMake (or GCC?) to insert RPATHs automatically at
# install time.
append_path LD_RUN_PATH "${GCC_LIB64_DIR}" "${CUDA_LIB_DIR}" "${ROCM_LIB64_DIR}" "${ROCM_LIB_DIR}" 
export CMAKE_PREFIX_PATH="${CUDA_HOME}:${ROCM_PREFIX}/llvm:${ROCM_PREFIX}"

# export LIBRARY_PATH="..." not set. CMake installation RPATHs are apparently not added to
#     libraries automatically when this variable is set.

show_build_env

###############
# ROCk Kernel Drivers

log_section "[CORE] Installing 'ROCk Kernel Drivers'"
log_info "Skipping ROCk install on PPC64le"


###############
# ROCt Thunk Interface

log_section "[CORE] Installing 'ROCt Thunk Interface'."

mkdir -p "${BUILD_PREFIX}/build/ROCT-Thunk-Interface"
cd "${BUILD_PREFIX}/build/ROCT-Thunk-Interface"
if [ -f "${ROCM_PREFIX}/lib64/libhsakmt.so" ]; then
  log_info "ROCt Thunk Interface is already installed; skipping"
else
  _ROCt_RPATH="${TOOLCHAIN_RPATH}"
  cmake -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"     \
        -DCMAKE_C_COMPILER="${GCC_BIN}"             \
        -DCMAKE_CXX_COMPILER="${GXX_BIN}"           \
        -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE    \
        -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR} \
        "${BUILD_PREFIX}/src/ROCT-Thunk-Interface"
  make -j "${THREADS:-16}"
  make install
  make install-dev
fi


###############
# ROCr HSA Runtime

log_section "[CORE] Installing 'ROCr Runtime' (HSA)."

append_path LD_RUN_PATH "${ROCM_PREFIX}/hsa/lib"

mkdir -p "${BUILD_PREFIX}/build/ROCR-Runtime"
cd "${BUILD_PREFIX}/build/ROCR-Runtime"
if [ -f "${ROCM_PREFIX}/hsa/lib/libhsa-runtime64.so" ]; then
  log_info "ROCr HSA Runtime is already installed; skipping."
else
  cmake -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"     \
        -DCMAKE_C_COMPILER="${GCC_BIN}"             \
        -DCMAKE_CXX_COMPILER="${GXX_BIN}"           \
        -HSAKMT_INC_PATH="${ROCM_PREFIX}/include"   \
        -HSAKMT_LIB_PATH="${ROCM_PREFIX}/lib64"     \
        -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE    \
        -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR}:${ROCR_LIB_DIR} \
        "${BUILD_PREFIX}/src/ROCR-Runtime/src"
  make -j "${THREADS:-16}"
  make install
fi


###############
# ROCm SMI utility

log_section "[SUPPORT] Installing 'ROCm SMI' utility."

# Pointless on an NVIDIA platform, but...
mkdir -p "${ROCM_PREFIX}/bin"
cp -a "${BUILD_PREFIX}/src/ROC-smi/rocm_smi.py" "${ROCM_PREFIX}/bin/rocm_smi.py"
ln -srf "${ROCM_PREFIX}/bin/rocm_smi.py" "${ROCM_PREFIX}/bin/rocm-smi"


###############
# CMake Utils

log_section "[SUPPORT] Installing 'ROCm-cmake' utility."

mkdir -p "${BUILD_PREFIX}/build/rocm-cmake"
cd "${BUILD_PREFIX}/build/rocm-cmake"
if [ -f "${ROCM_PREFIX}/share/rocm/cmake/ROCMAnalyzers.cmake" ]; then
  log_info "ROCm-cmake is already installed; skipping."
else
  cmake -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}" \
        -DCMAKE_C_COMPILER="${GCC_BIN}"         \
        -DCMAKE_CXX_COMPILER="${GXX_BIN}"       \
        -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR}:${ROCR_LIB_DIR} \
        "${BUILD_PREFIX}/src/rocm-cmake"
  make -j "${THREADS:-16}"
  make install
fi

append_path CMAKE_MODULE_PATH "${ROCM_PREFIX}/share/rocm/cmake"


###############
# rocminfo

# Pointless on an NVIDIA platform, but...
log_section "[SUPPORT] Installing 'rocminfo' utility."

cd "${BUILD_PREFIX}/src/rocminfo"
apply_patch_file "${PATCH_ROOT_DIR}/rocm-${ROCM_VERSION}-rocminfo-ppc64le.patch"

mkdir -p "${BUILD_PREFIX}/build/rocminfo"
cd "${BUILD_PREFIX}/build/rocminfo"

if [ -f "${ROCM_PREFIX}/bin/rocminfo" ]; then
  log_info "rocminfo is already installed; skipping."
else
  _ROCINFO_RPATH="${TOOLCHAIN_RPATH} ${ROCM_LIB64_RPATH} ${ROCM_LIB_RPATH} ${ROCR_RPATH}"
  cmake -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"    \
        -DROCM_DIR="${ROCM_PREFIX}"                \
        -DCMAKE_C_COMPILER="${GCC_BIN}"            \
        -DCMAKE_CXX_COMPILER="${GXX_BIN}"          \
        -DCMAKE_CXX_LINK_FLAGS="${_ROCINFO_RPATH}" \
        -DROCRTST_BLD_TYPE="Release"               \
        -DROCRTST_BLD_BITS="64"                    \
        -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE    \
        -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR}:${ROCR_LIB_DIR} \
        "${BUILD_PREFIX}/src/rocminfo"
  make
  make install
fi


###############
# ROCm Device Libraries 

log_section "[DEV] Installing 'ROCm Device Libraries'."

mkdir -p "${BUILD_PREFIX}/build/ROCm-Device-Libs"
cd "${BUILD_PREFIX}/build/ROCm-Device-Libs"
if [ -n "$(find "${ROCM_PREFIX}/lib" -maxdepth 1 -iname '*.bc')" ]; then
  log_info "ROCm-Device-Libs are already installed; skipping."
else
  cmake -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"  \
        -DCMAKE_C_COMPILER="${CLANG_BIN}"        \
        -DCMAKE_CXX_COMPILER="${CLANGXX_BIN}"    \
        -DLLVM_DIR="${LLVM_PREFIX}"              \
        -DROCM_DIR="${ROCM_PREFIX}"              \
        -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR}:${ROCR_LIB_DIR} \
        "${BUILD_PREFIX}/src/ROCm-Device-Libs"
  make -j "${THREADS:-16}"
  make install
fi


###############
# HCC

log_section "[DEV] Installing 'HCC'."

# FIXME (upstream): HCC is deprecated, but it's runtime APIs are needed by HIP.
# HCC necessarily includes *it's own build of llvm/clang* so now we have two
# LLVM compilers plus an out-of-band build of GCC. There doesn't appear to be a
# way to configure HCC's build to use an external LLVM/clang build, although I
# believe that should be permitted as I'd much prefer to use the build from
# above.

# WARNING: _Float16 not supported on ppc64le by clang, but __fp16 is. The
# kalmar_math.h header is modified to semi-roll back to the ppc64le-supported
# type without testing. 16-bit floats are probably borked in this build.
#
# See:
# https://clang.llvm.org/docs/LanguageExtensions.html#half-precision-floating-point
# https://github.com/RadeonOpenCompute/hcc/pull/865
# https://github.com/RadeonOpenCompute/hcc/pull/952
# https://github.com/RadeonOpenCompute/hcc/issues/1045

# WARNING: the `rocdl_links` custom CMake target isn't created when using
# external ROCm (ie, when passing `-DHCC_INTEGRATE_ROCDL=OFF`) but is needed by
# the the cmake-tests target for hccrt.

cd "${BUILD_PREFIX}/src/hcc"
apply_patch_file "${PATCH_ROOT_DIR}/rocm-${ROCM_VERSION}-hcc-ppc64le.patch"

mkdir -p "${BUILD_PREFIX}/build/hcc"
cd "${BUILD_PREFIX}/build/hcc"


if "${HCC_BIN}" --version > /dev/null 2>&1; then
  log_info "HCC already installed; skipping."
else
  cmake -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}/hcc" \
        -DCMAKE_BUILD_TYPE="Release"                \
        -DCMAKE_C_COMPILER="${GCC_BIN}"             \
        -DCMAKE_CXX_COMPILER="${GXX_BIN}"           \
        -DGCC_INSTALL_PREFIX="${LLVM_PREFIX}"       \
        -DCMAKE_CXX_FLAGS="${TOOLCHAIN_RPATH} -DLITTLEENDIAN_CPU" \
        -DROCM_ROOT="${ROCM_PREFIX}"                \
        -DHCC_INTEGRATE_ROCDL="OFF"                 \
        -DROCM_DEVICE_LIB_DIR="${ROCM_PREFIX}/lib"  \
        -DHSA_HEADER_DIR="${ROCM_PREFIX}/include"   \
        -DHSA_LIBRARY_DIR="${ROCM_PREFIX}/lib"      \
        -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE    \
        -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR}:${ROCR_LIB_DIR} \
        "${BUILD_PREFIX}/src/hcc"
#       -DCMAKE_CXX_LINK_FLAGS="${TOOLCHAIN_RPATH}" \
  make -j "${THREADS:-16}"
  make install
fi

prepend_path CMAKE_PREFIX_PATH "${ROCM_PREFIX}/hcc"


###############
# ROCm Code Object Manager (ROCm-CompilerSupport)

log_section "[DEV] Installing 'ROCm-CompilerSupport'."

mkdir -p "${BUILD_PREFIX}/build/ROCm-CompilerSupport"
cd "${BUILD_PREFIX}/build/ROCm-CompilerSupport"
if [ ! -f "${ROCM_PREFIX}/comgr/lib/libamd_comgr.so" ]; then
  cmake -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}/comgr"           \
        -DCMAKE_BUILD_TYPE=Release                              \
        -DCMAKE_C_COMPILER="${CLANG_BIN}"                       \
        -DCMAKE_CXX_COMPILER="${CLANGXX_BIN}"                   \
        -DROCM_DIR="${ROCM_PREFIX}"                             \
        -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE                \
        -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR}:${ROCR_LIB_DIR} \
        -DCMAKE_PREFIX_PATH="${ROCM_PREFIX};${LLVM_PREFIX};${ROCM_PREFIX}/lib" \
        "${BUILD_PREFIX}/src/ROCm-CompilerSupport/lib/comgr"
  make -j "${THREADS:-16}"
  make install
fi

prepend_path CMAKE_PREFIX_PATH "${ROCM_PREFIX}/comgr"

###############
# HIP

log_section "[DEV] Installing 'HIP'."

# WARNING: HIP, when building for the HCC platform uses _Float16 instead of
# __fp16 similar to hcc (see above hcc build notes). It has been patched to use
# ppc64le compatible types, though not all files have been patched (see, for
# example include/hip/hcc_detail/hip_fp16.h). Half-precision floats may be
# broken.
cd "${BUILD_PREFIX}/src/HIP"
apply_patch_file "${PATCH_ROOT_DIR}/rocm-${ROCM_VERSION}-hip-ppc64le.patch"

mkdir -p "${BUILD_PREFIX}/build/HIP"
cd "${BUILD_PREFIX}/build/HIP"

# HSA_PATH needed at buildtime only for HIP_PLATFORM=hcc, but still needed at
# runtime for both `hcc` and `nvcc` platforms

# The hip-config.cmake file used by downstream libraries is only installed when
# platform is hcc. AMD needs to write fully functional CMakeLists.txt in a
# platform agnostic way. Or actually heed their HCC deprecation warning and stop
# using the presence of hcc as the only way to fully install all required parts
# of each ROCm component. This is fucking ridiculous.
# https://github.com/ROCm-Developer-Tools/HIP/blob/3a7eb694f508517b3389ed8a9295bf41f8d16685/CMakeLists.txt#L384-L417

if [ -f "${ROCM_PREFIX}/hip/bin/hipcc" ]; then
  log_info "HIP is already installed."
else
  cmake -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}/hip" \
        -DCMAKE_BUILD_TYPE=Release                  \
        -DCMAKE_C_COMPILER="${CLANG_BIN}"           \
        -DCMAKE_CXX_COMPILER="${CLANGXX_BIN}"       \
        -DCMAKE_CXX_FLAGS=-DLITTLEENDIAN_CPU        \
        -DHSA_PATH="${ROCM_PREFIX}/hsa"             \
        -DHCC_HOME="${ROCM_PREFIX}/hcc"             \
        -DHIP_COMPILER="clang"                      \
        -DHIP_PLATFORM="hcc"                        \
        -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE    \
        -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR}:${ROCR_LIB_DIR} \
        "${BUILD_PREFIX}/src/HIP"
  make -j "${THREADS:-16}"
  make install

  # Reconfigure hip to use nvcc platform
  [ -f ${ROCM_PREFIX}/hip/lib/.hipInfo ] \
    && [ ! -f ${ROCM_PREFIX}/hip/lib/.hipInfo.orig ] \
    && sed -i.orig \
          's|^\([^#]*=\)\(clang\)|# \1\2\n\1nvcc|g' \
          ${ROCM_PREFIX}/hip/lib/.hipInfo
fi

export ROCM_PATH="${ROCM_PREFIX}"
export HIP_PATH="${ROCM_PREFIX}/hip"
export HSA_PATH="${ROCM_PREFIX}/hsa"
export HCC_PATH="${ROCM_PREFIX}/hcc"
export HIP_CLANG_PATH="${ROCM_PREFIX}/llvm/bin"
export DEVICE_LIB_PATH="${ROCM_PREFIX}/lib"
export HIP_PLATFORM=nvcc

prepend_path PATH "${ROCM_PREFIX}/hip/bin"
prepend_path CMAKE_PREFIX_PATH "${ROCM_PREFIX}/hip"
append_path CMAKE_MODULE_PATH "${ROCM_PREFIX}/hip/cmake"

log_info "HIP configuration start ===================="
hipconfig --full
log_info "HIP configuration end   ===================="


###############
# hipBLAS

log_section "[LIB] Installing 'hipBLAS'"

mkdir -p "${BUILD_PREFIX}/build/hipBLAS"
cd "${BUILD_PREFIX}/build/hipBLAS"

if [ -f "${ROCM_PREFIX}/hipblas/lib/libhipblas.so" ]; then
  log_info "hipBLAS is already installed; skipping."
else
  cmake -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"        \
        -DCMAKE_BUILD_TYPE=Release                     \
        -DCMAKE_C_COMPILER=${CLANG_BIN}                \
        -DCMAKE_CXX_COMPILER=${CLANGXX_BIN}            \
        -DROCM_DIR="${ROCM_PREFIX}"                    \
        -Dhip_DIR="${ROCM_PREFIX}/hip"                 \
        -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE       \
        -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR}:${ROCR_LIB_DIR} \
        "${BUILD_PREFIX}/src/hipBLAS"
  make -j "${THREADS:-16}"
  make install
fi

prepend_path CMAKE_PREFIX_PATH "${ROCM_PREFIX}/hipblas"
# append_path CMAKE_MODULE_PATH "${ROCM_PREFIX}/lib/cmake/hipblas"


###############
# rocFFT

# FIXME: Buildable only on HCC/AMD platforms, but provides hipFFT for NVCC
# platforms. See detailed errors below along with 
#  - https://github.com/ROCmSoftwarePlatform/rocFFT/issues/276
#  - https://github.com/ROCmSoftwarePlatform/rocFFT/issues/271
#  - https://github.com/ROCmSoftwarePlatform/rocFFT/issues/134
#  - https://github.com/ROCm-Developer-Tools/HIP/issues/1521
#      (Related errors with latest versions of CMake)

# Cannot build on nvcc platform using clang (v10 incompatible with CUDA).
# Hardcoded clang flags need to be patched to use GCC
# On NVCC platforms, no constructors for CUDA vector types double to double2 and float to float2:
#
## ```
## /gpfs/alpine/stf007/proj-shared/belhorn/rocm/2.10.0/src/rocFFT/library/src/device/real2complex.cpp(393): error: no suitable user-defined conversion from "float2" to "double2" exists
##           detected during:
##             instantiation of "void real_post_process_kernel<T,IN_PLACE>(size_t, size_t, size_t, T *, size_t, T *, size_t, const T *) [with T=double2, IN_PLACE=false]" 
## (499): here
##             instantiation of "void real_1d_pre_post_process<Tcomplex,R2C>(size_t, size_t, Tcomplex *, Tcomplex *, Tcomplex *, size_t, size_t, size_t, size_t, size_t, hipStream_t) [with Tcomplex=double2, R2C=true]" 
## (590): here
##             instantiation of "void real_1d_pre_post<R2C>(const void *, void *) [with R2C=true]" 
## (596): here
## ```


log_section "[LIB] Installing 'rocFFT'"

cd "${BUILD_PREFIX}/src/rocFFT"
apply_patch_file "${PATCH_ROOT_DIR}/rocm-${ROCM_VERSION}-rocFFT-ppc64le.patch"

mkdir -p "${BUILD_PREFIX}/build/rocFFT"
cd "${BUILD_PREFIX}/build/rocFFT"

# Disabled rocFFT build due to issues listed above.
if false; then
  _ROCFFT_RPATH="-Xlinker='-rpath,${LLVM_PREFIX}/lib64' -L${LLVM_PREFIX}/lib64"
  _ROCFFT_RPATH+=" -Xlinker='-rpath,${CUDA_LIB_DIR}' -L${CUDA_LIB_DIR}"
  #     -DCUDA_HOST_COMPILER="${GXX_BIN}"                \
  #     -DCUDA_PROPAGATE_HOST_FLAGS=OFF                  \
  cmake                                                  \
    -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"              \
    -DCMAKE_BUILD_TYPE=Release                           \
    -DCMAKE_CXX_COMPILER_ID=NVIDIA                       \
    -DCMAKE_CXX_FLAGS="--verbose -std=c++11"             \
    -DCMAKE_CXX_LINK_FLAGS="${_ROCFFT_RPATH}"            \
    -DCMAKE_CXX_COMPILER="${ROCM_PREFIX}/hip/bin/hipcc"  \
    -DNVCUDASAMPLES_ROOT="${CUDA_HOME}/samples"          \
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE             \
    -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR}:${ROCR_LIB_DIR} \
    "${BUILD_PREFIX}/src/rocFFT"
  make -j "${DISABLE_THREADS:-1}" VERBOSE=1
else
  log_info "Cannot currently build rocFFT on NVCC platforms; skipping."
fi


###############
# rocRAND

log_section "[LIB] Installing 'rocRAND'"

mkdir -p "${BUILD_PREFIX}/build/rocRAND"
cd "${BUILD_PREFIX}/build/rocRAND"

if [ -f "${ROCM_PREFIX}/rocrand/lib/librocrand.so" ]; then
  log_info "rocRAND is already installed; skipping."
else
  _ROCRAND_RPATH="${TOOLCHAIN_RPATH} ${CUDA_RPATH}"
  cmake                                                      \
    -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"                  \
    -DCMAKE_BUILD_TYPE=Release                               \
    -DHIP_PLATFORM=nvcc                                      \
    -DHIP_PATH=${HIP_PATH}                                   \
    -DCMAKE_CXX_COMPILER="${GXX_BIN}"                        \
    -DCMAKE_CXX_LINK_FLAGS="${_ROCRAND_RPATH}"               \
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE                 \
    -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR}:${ROCR_LIB_DIR} \
    "${BUILD_PREFIX}/src/rocRAND"
  make -j "${THREADS:-16}"
  make install
fi

prepend_path CMAKE_PREFIX_PATH "${ROCM_PREFIX}/hiprand" "${ROCM_PREFIX}/rocrand"
# append_path CMAKE_MODULE_PATH "${ROCM_PREFIX}/rocrand/lib/cmake/rocrand" \
#                               "${ROCM_PREFIX}/hiprand/lib/cmake/hiprand"


###############
# hipSPARSE

log_section "[LIB] Installing 'hipSPARSE'"

mkdir -p "${BUILD_PREFIX}/build/hipSPARSE"
cd "${BUILD_PREFIX}/build/hipSPARSE"

if [ -f "${ROCM_PREFIX}/hipsparse/lib/libhipsparse.so" ]; then
  log_info "hipSPARSE is already installed; skipping."
else
  cmake                                         \
    -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"     \
    -DCMAKE_BUILD_TYPE=Release                  \
    -DHIP_ROOT_DIR=${HIP_PATH}                  \
    -DCMAKE_CXX_COMPILER=${GXX_BIN}             \
    -DBUILD_CUDA=ON                             \
    -DBUILD_CLIENTS_SAMPLES=OFF                 \
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE    \
    -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR}:${ROCR_LIB_DIR} \
    "${BUILD_PREFIX}/src/hipSPARSE"
  make -j "${THREADS:-16}"
  make install
fi

prepend_path CMAKE_PREFIX_PATH "${ROCM_PREFIX}/hipsparse"


###############
# hipCUB

log_section "[LIB] Installing 'hipCUB'"

# - v3.0.0: Fails to build without adding build-dir GTest libs to the global
#           LD_LIBRARY_PATH. Also requires same patch as ROCm v2 to remove the
#           hard dependency on ROCm v1.5.
# - v2.X: Has hard requirement in CMake on ROCm v1.5. Patching that out allows
#         for a successful build, but is concerning.


cd "${BUILD_PREFIX}/src/hipCUB"
apply_patch_file "${PATCH_ROOT_DIR}/rocm-${ROCM_VERSION}-hipCUB-ppc64le.patch"

mkdir -p "${BUILD_PREFIX}/build/hipCUB"
cd "${BUILD_PREFIX}/build/hipCUB"

append_path LD_LIBRARY_PATH "${BUILD_PREFIX}/build/hipCUB/gtest/lib64"

if [ -f "${ROCM_PREFIX}/hipcub/include/hipcub/hipcub.hpp" ]; then
  log_info "hipCUB is already installed; skipping."
else
  cmake                                         \
    -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"     \
    -DCMAKE_BUILD_TYPE=Release                  \
    -DCMAKE_CXX_COMPILER=${GXX_BIN}             \
    -DHIP_PLATFORM=nvcc                         \
    -DBUILD_TEST=ON                             \
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE    \
    -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR}:${ROCR_LIB_DIR} \
    "${BUILD_PREFIX}/src/hipCUB"
  make -j "${THREADS:-16}"
  make install
fi


###############
#  Exit Now - the following components haven't been built successfully yet.

log_section "Exit early."
exit 0


###############
# rocALUTION

log_section "[LIB] Installing 'rocALUTION'"

mkdir -p "${BUILD_PREFIX}/build/rocALUTION"
cd "${BUILD_PREFIX}/build/rocALUTION"

cmake                                         \
  -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"     \
  -DCMAKE_BUILD_TYPE=Release                  \
  "${BUILD_PREFIX}/src/rocALUTION"
make -j "${THREADS:-16}"


###############
# MIOpenGEMM

log_section "[LIB] Installing 'MIOpenGEMM'"

mkdir -p "${BUILD_PREFIX}/build/MIOpenGEMM"
cd "${BUILD_PREFIX}/build/MIOpenGEMM"

cmake                                         \
  -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"     \
  -DCMAKE_BUILD_TYPE=Release                  \
  "${BUILD_PREFIX}/src/MIOpenGEMM"
make -j "${THREADS:-16}"


###############
# MIOpen

log_section "[LIB] Installing 'MIOpen'"

mkdir -p "${BUILD_PREFIX}/build/MIOpen"
cd "${BUILD_PREFIX}/build/MIOpen"

cmake                                         \
  -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"     \
  -DCMAKE_BUILD_TYPE=Release                  \
  "${BUILD_PREFIX}/src/MIOpen"
make -j "${THREADS:-16}"


###############
# rocThrust

log_section "[LIB] Installing 'rocThrust'"

mkdir -p "${BUILD_PREFIX}/build/rocThrust"
cd "${BUILD_PREFIX}/build/rocThrust"

cmake                                         \
  -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"     \
  -DCMAKE_BUILD_TYPE=Release                  \
  "${BUILD_PREFIX}/src/rocThrust"
make -j "${THREADS:-16}"


###############
# ROCm SMI Lib

log_section "[LIB] Installing 'rocm_smi_lib'"

mkdir -p "${BUILD_PREFIX}/build/rocm_smi_lib"
cd "${BUILD_PREFIX}/build/rocm_smi_lib"

cmake                                         \
  -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"     \
  -DCMAKE_BUILD_TYPE=Release                  \
  "${BUILD_PREFIX}/src/rocm_smi_lib"
make -j "${THREADS:-16}"


###############
# RCCL

log_section "[LIB] Installing 'rccl'"

mkdir -p "${BUILD_PREFIX}/build/rccl"
cd "${BUILD_PREFIX}/build/rccl"

cmake                                         \
  -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"     \
  -DCMAKE_BUILD_TYPE=Release                  \
  "${BUILD_PREFIX}/src/rccl"
make -j "${THREADS:-16}"


###############
# MIVisionX

log_section "[LIB] Installing 'MIVisionX'"

mkdir -p "${BUILD_PREFIX}/build/MIVisionX"
cd "${BUILD_PREFIX}/build/MIVisionX"

cmake                                         \
  -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"     \
  -DCMAKE_BUILD_TYPE=Release                  \
  "${BUILD_PREFIX}/src/MIVisionX"
make -j "${THREADS:-16}"


###############
# ATMI - Async Task & Memory Interface

log_section "[DEV] Installing 'ATMI'."

mkdir -p "${BUILD_PREFIX}/build/atmi"
cd "${BUILD_PREFIX}/build/atmi"
cmake -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"     \
      -DCMAKE_C_COMPILER="${GCC_BIN}"             \
      -DCMAKE_CXX_COMPILER="${GXX_BIN}"           \
      -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE    \
      -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR} \
      "${BUILD_PREFIX}/src/atmi"
make -j "${THREADS:-16}"


###############
# ROCr Debug Agent

log_section "[DEV] Installing 'rocr_debug_agent'."

mkdir -p "${BUILD_PREFIX}/build/rocr_debug_agent"
cd "${BUILD_PREFIX}/build/rocr_debug_agent"
cmake -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"     \
      -DCMAKE_C_COMPILER="${GCC_BIN}"             \
      -DCMAKE_CXX_COMPILER="${GXX_BIN}"           \
      -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE    \
      -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR} \
      "${BUILD_PREFIX}/src/rocr_debug_agent"
make -j "${THREADS:-16}"


###############
# rocprofiler

log_section "[DEV] Installing 'rocprofiler'."

mkdir -p "${BUILD_PREFIX}/build/rocprofiler"
cd "${BUILD_PREFIX}/build/rocprofiler"
cmake -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"     \
      -DCMAKE_C_COMPILER="${GCC_BIN}"             \
      -DCMAKE_CXX_COMPILER="${GXX_BIN}"           \
      -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE    \
      -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR} \
      "${BUILD_PREFIX}/src/rocprofiler"
make -j "${THREADS:-16}"

###############
# roctracer

log_section "[DEV] Installing 'roctracer'."

mkdir -p "${BUILD_PREFIX}/build/roctracer"
cd "${BUILD_PREFIX}/build/roctracer"
cmake -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"     \
      -DCMAKE_C_COMPILER="${GCC_BIN}"             \
      -DCMAKE_CXX_COMPILER="${GXX_BIN}"           \
      -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE    \
      -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR} \
      "${BUILD_PREFIX}/src/roctracer"
make -j "${THREADS:-16}"


###############
# The unbuildable components.

log_section "Finished all components buildable on NVCC platforms."
exit 0


###############
# rocBLAS
# XXX: rocBLAS is buildable on HCC/AMD platforms only.
# This package is replaced by hipBLAS on NVCC platforms.

log_section "[LIB] Installing 'rocBLAS'"

mkdir -p "${BUILD_PREFIX}/build/rocBLAS"
cd "${BUILD_PREFIX}/build/rocBLAS"

cmake -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"       \
      -DCMAKE_BUILD_TYPE=Release                    \
      -DCMAKE_C_COMPILER=${CLANG_BIN}               \
      -DCMAKE_CXX_COMPILER=${CLANGXX_BIN}           \
      "${BUILD_PREFIX}/src/rocBLAS"
make -j "${THREADS:-16}" VERBOSE=1


###############
# rocPRIM
# XXX: rocPRIM (and dependency rocSPARSE) are for HCC/AMD platforms only.
# This package is replaced by hipSPARSE

log_section "[LIB] Installing 'rocPRIM'"

cd "${BUILD_PREFIX}/src/rocPRIM"
apply_patch_file "${PATCH_ROOT_DIR}/rocm-${ROCM_VERSION}-rocPRIM-ppc64le.patch"

mkdir -p "${BUILD_PREFIX}/build/rocPRIM"
cd "${BUILD_PREFIX}/build/rocPRIM"

cmake                                                  \
  -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"              \
  -DCMAKE_BUILD_TYPE=Release                           \
  -DCMAKE_CXX_COMPILER="${HCC_BIN}"                    \
  -DHIP_PLATFORM="hcc"                                 \
  -DHIP_PATH=${HIP_PATH}                               \
  -DCMAKE_CXX_FLAGS="-D__HIP_PLATFORM_HCC__ -DLITTLEENDIAN_CPU" \
  -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE             \
  -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR}:${ROCR_LIB_DIR} \
  "${BUILD_PREFIX}/src/rocPRIM"
make -j "${THREADS:-16}"


###############
# rocSPARSE
# XXX: rocSPARSE (and dependency rocPRIM) are for HCC/AMD platforms only.
# This package is replaced by hipSPARSE

log_section "[LIB] Installing 'rocSPARSE'"

mkdir -p "${BUILD_PREFIX}/build/rocSPARSE"
cd "${BUILD_PREFIX}/build/rocSPARSE"

cmake                                                  \
  -DCMAKE_INSTALL_PREFIX="${ROCM_PREFIX}"              \
  -DCMAKE_BUILD_TYPE=Release                           \
  -DCMAKE_CXX_COMPILER="${HCC_BIN}"                    \
  -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE             \
  -DCMAKE_INSTALL_RPATH=${GCC_LIB64_DIR}:${CUDA_LIB_DIR}:${ROCM_LIB64_DIR}:${ROCM_LIB_DIR}:${ROCR_LIB_DIR} \
  "${BUILD_PREFIX}/src/rocSPARSE"
make -j "${THREADS:-16}"
