module purge
module load cuda
ROCM_VERSION="${ROCM_VERSION:-3.0.0}"
ROCM_PREFIX="/sw/.testing/belhorn/summit/rocm/${ROCM_VERSION}/opt/rocm"
export ROCM_PATH=${ROCM_PREFIX}
export HIP_PATH=${ROCM_PREFIX}/hip 
export HSA_PATH=${ROCM_PREFIX}/hsa
export HCC_PATH=${ROCM_PREFIX}/hcc
export HIP_PLATFORM=nvcc
export HIPCC_VERBOSE=7
export DEVICE_LIB_PATH=${ROCM_PREFIX}/lib
export CUDA_PATH="${CUDA_DIR}"
export PATH="${ROCM_PREFIX}/hip/bin:${ROCM_PREFIX}/bin:${ROCM_PREFIX}/llvm/bin:${PATH}"
